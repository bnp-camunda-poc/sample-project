/**
 * 
 */
package com.camunda.slv.config;

/**
 * @author selvark
 *
 */
public class Constants {
public static final String loginRequested="loginRequested";
public static final String registeredUser = "registeredUser";
public static final String newUser = "newUser";
public static final String successfulValidation = "successfulValidation";
public static final String validationFailed = "validationFailed";
public static final String uploadDoc = "uploadDoc";
public static final String employeeValidation = "employeeValidation";
public static final String docRejected = "docRejected";
public static final String documentOrCustomerAcceptance = "documentOrCustomerAcceptanceSet";
}
