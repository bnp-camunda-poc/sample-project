/**
 * 
 */
package com.camunda.slv.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;

/**
 * @author selvark
 *
 */

@Entity
@Table(name = "user_entity")
@Getter
@Setter
@AllArgsConstructor
@ToString
@NoArgsConstructor
public class UserEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long id;

	@Column(name = "firstName")
	public String firstName;

	@Column(name = "familyName")
	public String familyName;

	@Column(name = "dob")
	public String dob;

	@Column(name = "nationality")
	private String nationality;
	@Column(name = "gender")
	private String gender;
	@Column(name = "language")
	private String language;
	@Column(name = "address")
	private String address;
	@Column(name = "idCardNumber")
	private String idCardNumber;
	@Column(name = "idCardValidity")
	private String idCardValidity;
	@Column(name = "residenceStatus")
	private String residenceStatus;
	@Column(name = "slvStatus")
	private String slvStatus;
	@Column(name = "businessKey")
	private String businessKey;

}