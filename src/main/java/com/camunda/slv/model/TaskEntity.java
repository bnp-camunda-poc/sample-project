/**
 * 
 */
package com.camunda.slv.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * @author selvark
 *
 */

@Entity
@Table(name = "task_entity")
@Getter
@Setter
@AllArgsConstructor
@ToString
@NoArgsConstructor
public class TaskEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "customerId")
	public String customerId;

	@Column(name = "name")
	public String name;

	@Column(name = "creationDate")
	public String creationDate;

	@Column(name = "taskStatus")
	private String taskStatus;
	

}