/**
 * 
 */
package com.camunda.slv.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * @author selvark
 *
 */
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class UserDetails {

	private String firstName;
	private String familyName;
	private String dob;
	private String nationality;
	private String gender;
	private String language;
	private String address;
	private String idCardNumber;
	private String idCardValidity;
	private String residenceStatus;
}
