/**
 * 
 */
package com.camunda.slv.model;

import java.sql.Blob;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * @author selvark
 *
 */

@Entity
@Table(name = "document_entity")
@Getter
@Setter
@AllArgsConstructor
@ToString
@NoArgsConstructor
public class DocumentEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Column(name = "isCardElectronic")
	private String isCardElectronic;
	@Column(name = "customerId")
	private String customerId;

	@Lob
	@Column(name = "idCard")
	private Blob idCard;
	@Lob
	@Column(name = "addressProof")
	private Blob addressProof;
	@Column(name = "docStatus")
	private String docStatus;
}
