/**
 * 
 */
package com.camunda.slv.operation;

import java.util.concurrent.CompletableFuture;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.ToString;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * @author selvark
 *
 */
@Component
@Async
public class CamundaProcess {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(CamundaProcess.class);
	@Value("${camunda.url}")
	private String camundaURL;

	ObjectMapper mapper = new ObjectMapper();

	@Async
	public CompletableFuture<Boolean> camundaMessage(String messageName,String businessKey,String operationName, String operationStatus)
			throws InterruptedException, JsonMappingException, JsonProcessingException {
		
		LOGGER.info("Sending message {} with business key constraint {}", messageName, businessKey);

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		RestMessageDto input =new RestMessageDto(messageName, businessKey, operationName, operationStatus);
		HttpEntity<RestMessageDto> entity = new HttpEntity<RestMessageDto>(//
				input, // here is the JSON body
				headers);
		LOGGER.info("Json======",mapper.writeValueAsString(input).toString());
		 
		ResponseEntity<Object> result = new RestTemplate().postForEntity(camundaURL, entity, null);

		if (!result.getStatusCode().is2xxSuccessful()) {
			throw new RuntimeException(result.getStatusCode().toString() + ": " + result.getBody());
		} else {
			LOGGER.info("Response code", result.getStatusCode().toString());
			return CompletableFuture.completedFuture(true);
		}
	}
	
	@Async
	public CompletableFuture<Boolean> simpleMessage(String messageName, String businessKey)
			throws InterruptedException, JsonMappingException, JsonProcessingException{
		return simpleMessage(messageName, businessKey, false);
	}
	
	@Async
	public CompletableFuture<Boolean> simpleMessage(String messageName, String businessKey, boolean isMultiple)
			throws InterruptedException, JsonMappingException, JsonProcessingException{
		LOGGER.info("Sending message {} with business key constraint {}", messageName, businessKey);
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		RestMessageDto data = new RestMessageDto(messageName, businessKey, "stage", messageName);
		HttpEntity<RestMessageDto> httpEntity = new HttpEntity<RestMessageDto>(
					data,
					headers
				);
		
		LOGGER.info("Json======",mapper.writeValueAsString(data).toString());
		
		ResponseEntity<Object> result = new RestTemplate().postForEntity(camundaURL, httpEntity, null);

		if (!result.getStatusCode().is2xxSuccessful()) {
			throw new RuntimeException(result.getStatusCode().toString() + ": " + result.getBody());
		} else {
			LOGGER.info("Response code", result.getStatusCode().toString());
			return CompletableFuture.completedFuture(true);
		}
	}
}
