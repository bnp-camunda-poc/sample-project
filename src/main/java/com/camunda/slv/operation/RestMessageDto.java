/**
 * 
 */
package com.camunda.slv.operation;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * @author selvark
 *
 */

@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class RestMessageDto {
	private String messageName;
	private String businessKey;
	private String operationName;
	private String operationStatus;


}

