/**
 * 
 */
package com.camunda.slv.controller;

import java.util.List;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.camunda.slv.model.DocumentEntity;
import com.camunda.slv.model.ResponseEnvelope;
import com.camunda.slv.model.TaskEntity;
import com.camunda.slv.model.UserDetails;
import com.camunda.slv.model.UserEntity;

/**
 * @author selvark
 *
 */

@RequestMapping("/v1")
public interface ISLVProcess {
	@CrossOrigin
	@GetMapping(value = "/login")
	public ResponseEntity<ResponseEnvelope> login(@RequestParam String firstName, @RequestParam String familyName, @RequestParam String dob,@RequestParam String id);
	@CrossOrigin
	@PostMapping(value = "/form")
	public ResponseEntity<ResponseEnvelope> processForm(@RequestBody UserDetails userInfo,@RequestParam String id);
	@CrossOrigin
	@PostMapping("/upload")
	public ResponseEntity<String> uploadFile(@RequestParam("idCard") MultipartFile idCard,@RequestParam("addressProof") MultipartFile addressProof,@RequestParam("isElectronic") String isElectronic,@RequestParam("customerId") String customerId,@RequestParam String id);
	@CrossOrigin
	@GetMapping("/autoValidate")
	public String autoValidation(@RequestParam("businessKey") String businessKey);
	@CrossOrigin
	@PostMapping("/backOffice")
	public ResponseEntity<String> manualValidation(@RequestParam("docStatus") String docStatus,@RequestParam String id,@RequestParam("customerId") String customerId);
    @GetMapping("/getTaskList")
    @CrossOrigin
    public List<TaskEntity> getTaskList(@RequestParam(defaultValue="0") Integer page,@RequestParam(defaultValue="${page.size}") Integer size);
    @GetMapping("/getCustomer")
    @CrossOrigin
	public UserEntity getCustomer(@RequestParam("customerId") String id);
    @GetMapping("/getDocuments")
    @CrossOrigin
    public DocumentEntity getDocument(@RequestParam("customerId")String customerId);
    @RequestMapping(value = "/getDocuments/{customerId}/", method = RequestMethod.GET, produces = MediaType.APPLICATION_PDF_VALUE )
    @CrossOrigin
    public @ResponseBody byte[] downloadDocument(@PathVariable("customerId")String customerId, @RequestParam("file")String file);
    @PostMapping("/documentOrCustomerAcceptance")
    @CrossOrigin
	String documentOrCustomerAcceptance(@RequestParam String businessKey, @RequestParam String taskId, @RequestParam String status);
    @GetMapping("/documentOrSLVStatusCheck")
    @CrossOrigin
	String documentOrSLVStatusCheck(@RequestParam String businessKey);
}
