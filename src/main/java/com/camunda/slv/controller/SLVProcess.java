/**
 * 
 */
package com.camunda.slv.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.camunda.slv.model.DocumentEntity;
import com.camunda.slv.model.ResponseEnvelope;
import com.camunda.slv.model.TaskEntity;
import com.camunda.slv.model.UserDetails;
import com.camunda.slv.model.UserEntity;
import com.camunda.slv.service.IProcessService;

/**
 * @author selvark
 *
 */

@RestController
public class SLVProcess implements ISLVProcess {

	@Autowired
	IProcessService processService;

	@Override
	public ResponseEntity<ResponseEnvelope> login(String firstName, String familyName, String dob,String id) {
		// TODO Auto-generated method stub

		return ResponseEntity.status(HttpStatus.OK)
				.body(processService.checkOnboardingStatus(firstName, familyName, dob,id));
	}

	@Override
	public ResponseEntity<ResponseEnvelope> processForm(UserDetails userInfo,String id) {

		return ResponseEntity.status(HttpStatus.OK).body(processService.saveForm(userInfo,id));
	}

	@Override
	public ResponseEntity<String> uploadFile(MultipartFile idCard, MultipartFile addressProof, String isElectronic,String customerId,String id) {
		try {

			return ResponseEntity.status(HttpStatus.OK)
					.body(processService.storeDoc(idCard, addressProof, isElectronic,customerId,id));
		} catch (Exception e) {

			return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body("Document upload failed");
		}
	}

	

	@Override
	public ResponseEntity<String> manualValidation(String docStatus, String id, String customerId) {
		return ResponseEntity.status(HttpStatus.OK)
				.body(processService.docStatus(docStatus,customerId,id));
	}

	@Override
	public String autoValidation(String businessKey) {
		// TODO Auto-generated method stub
		return processService.autoValidation(businessKey);
	}

	@Override
	public List<TaskEntity> getTaskList(Integer page, Integer size) {
		return processService.retrieveTask(page,size);
	}

	@Override
	public UserEntity getCustomer(String id) {
		return processService.getCustomer(id);
	}
	
	public DocumentEntity getDocument(String customerId) {
		return processService.getDocuments(customerId);
	}
	
	public byte[] downloadDocument(String customerId, String file) {
		return processService.downloadDocument(customerId, file);
	}

	public String documentOrCustomerAcceptance(String businessKey, String taskId, String status) {
		return processService.documentOrCustomerAcceptance(businessKey, taskId, status);
	}
	
	public String documentOrSLVStatusCheck(String businessKey) {
		return processService.documentOrSLVStatusCheck(businessKey);
	}
	
}
