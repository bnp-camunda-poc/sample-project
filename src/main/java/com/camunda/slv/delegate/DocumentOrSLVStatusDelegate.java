package com.camunda.slv.delegate;


import java.util.Optional;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.beans.factory.annotation.Autowired;

import com.camunda.slv.converter.BusinessConstants;
import com.camunda.slv.converter.UserConverter;
import com.camunda.slv.model.DocumentEntity;
import com.camunda.slv.model.UserEntity;
import com.camunda.slv.repo.DocumentRepo;
import com.camunda.slv.repo.TaskRepo;
import com.camunda.slv.repo.UserRepo;

public class DocumentOrSLVStatusDelegate implements JavaDelegate {

	@Autowired
	UserRepo userRepo;
	@Autowired
	DocumentRepo docRepo;
	@Autowired
	TaskRepo taskRepo;
	
	@Override
	public void execute(DelegateExecution execution) throws Exception {
		
		String result = BusinessConstants.CUSTOMER_REJECTED;

		Optional<UserEntity> user = userRepo.findByBusinessKey(execution.getBusinessKey());
		if(user.isPresent()) {
			UserEntity tempUser = user.get();
			if(tempUser.getSlvStatus() == (result = BusinessConstants.CUSTOMER_REJECTED)) {
				
				return;
			}

			Optional<DocumentEntity> doc = docRepo.findByCustomerId(tempUser.getId().toString());
			if(doc.isPresent()) {
				DocumentEntity tempDoc = doc.get();
				if(tempDoc.getDocStatus() == (result = BusinessConstants.DOCUMENT_ACCEPTED)) {
					return;
				} else if(tempDoc.getDocStatus() == (result = BusinessConstants.DOCUMENT_REJECTED)) {
					return;
				}
			}
		}
		
		execution.setVariable("status", result);
	}
	
}
