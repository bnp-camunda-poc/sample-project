/**
 * 
 */
package com.camunda.slv.converter;

/**
 * @author selvark
 *
 */
public class BusinessConstants {
public static final String COUNTRY="BELGIUM";
public static final String SUBMITED = "SUBMITED";
public static final String AUTOVALIDATED = "AUTO VALIDATED";
public static final String REST_ENDPOINT = "http://localhost:8081/rest/message";
public static final String Validated = "Validated";
public static final String YES = "YES";
public static final String Rejected = "Rejected";
public static final String DOCUMENT_ACCEPTED = "DOCUMENT_ACCEPTED";
public static final String DOCUMENT_REJECTED = "DOCUMENT_REJECTED";
public static final String CUSTOMER_REJECTED = "REJECTED";
public static final String TASK_COMPLETED = "COMPLETED";
}
