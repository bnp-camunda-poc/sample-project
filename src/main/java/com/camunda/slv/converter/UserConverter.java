/**
 * 
 */
package com.camunda.slv.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.camunda.slv.model.UserDetails;
import com.camunda.slv.model.UserEntity;

/**
 * @author selvark
 *
 */
@Component
public class UserConverter implements Converter<UserDetails,UserEntity>{

	@Override
	public UserEntity convert(UserDetails source) {
		UserEntity userEntity=new UserEntity();
		userEntity.setAddress(source.getAddress());
		userEntity.setDob(source.getDob());
		userEntity.setFamilyName(source.getFamilyName());
		userEntity.setFirstName(source.getFirstName());
		userEntity.setGender(source.getGender());
		userEntity.setIdCardNumber(source.getIdCardNumber());
		userEntity.setIdCardValidity(source.getIdCardValidity());
		userEntity.setLanguage(source.getLanguage());
		userEntity.setNationality(source.getNationality());
		userEntity.setResidenceStatus(source.getResidenceStatus());
		return userEntity;
	}

}
