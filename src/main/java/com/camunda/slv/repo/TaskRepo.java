/**
 * 
 */
package com.camunda.slv.repo;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.camunda.slv.model.TaskEntity;

/**
 * @author selvark
 *
 */

@Repository
public interface TaskRepo extends JpaRepository<TaskEntity, Long> {

	Page<TaskEntity> findByCustomerId(String customerId, Pageable paging);
}
