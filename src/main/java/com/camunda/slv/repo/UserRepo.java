/**
 * 
 */
package com.camunda.slv.repo;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.camunda.slv.model.UserEntity;

/**
 * @author selvark
 *
 */

@Repository
public interface UserRepo extends JpaRepository<UserEntity, Long> {

	UserEntity findByFirstName(String firstName);

	UserEntity findByFirstNameAndFamilyName(String firstName, String familyName);

	UserEntity findByFirstNameAndFamilyNameAndDob(String firstName, String familyName, String dob);

	Optional<UserEntity> findByBusinessKey(String businsessKey);

	//Optional<UserEntity> findByfirstNameAndfamilyNameAndDOB(String firstName, String familyName, String dob);

	//UserEntity findAllByfirstNameAndfamilyNameAndDOB(String firstName, String familyName, String dob);

}