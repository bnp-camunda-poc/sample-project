/**
 * 
 */
package com.camunda.slv.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.camunda.slv.model.DocumentEntity;
import java.util.Optional;

/**
 * @author selvark
 *
 */

@Repository
public interface DocumentRepo extends JpaRepository<DocumentEntity, Long> {
	 @Query("select a from DocumentEntity a where a.customerId=?1")
	 public Optional<DocumentEntity> findByCustomerId(String customerId);
}