/**
 * 
 */
package com.camunda.slv.service;

import java.sql.Blob;
import java.sql.SQLException;
import java.text.DateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.sql.rowset.serial.SerialBlob;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.camunda.slv.config.Constants;
import com.camunda.slv.converter.BusinessConstants;
import com.camunda.slv.converter.UserConverter;
import com.camunda.slv.model.DocumentEntity;
import com.camunda.slv.model.ResponseEnvelope;
import com.camunda.slv.model.TaskEntity;
import com.camunda.slv.model.UserDetails;
import com.camunda.slv.model.UserEntity;
import com.camunda.slv.operation.CamundaProcess;
import com.camunda.slv.operation.ValueDetails;
import com.camunda.slv.repo.DocumentRepo;
import com.camunda.slv.repo.TaskRepo;
import com.camunda.slv.repo.UserRepo;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;

/**
 * @author selvark
 *
 */
@Service
public class ProcessService implements IProcessService {
	private static final Logger LOGGER = LoggerFactory.getLogger(ProcessService.class);

	@Autowired
	UserRepo userRepo;
	@Autowired
	DocumentRepo docRepo;
	@Autowired
	UserConverter userConverter;
	@Autowired
	CamundaProcess process;
	@Autowired
	TaskRepo taskRepo;

	@Override
	public ResponseEnvelope checkOnboardingStatus(String firstName, String familyName, String dob, String id) {
		ResponseEnvelope response = new ResponseEnvelope();

		try {
			LOGGER.info("Process result",
					process.camundaMessage(Constants.loginRequested, id, "userLogin", "loginSubmitted"));
			UserEntity user = userRepo.findByFirstNameAndFamilyNameAndDob(firstName, familyName, dob);
			if (user != null) {

				response.setStatus("Found");

				response.setCustomerId(String.valueOf(user.getId()));
				LOGGER.info("Process result",
						process.camundaMessage(Constants.registeredUser, id, "userLogin", "loginSuccess"));
			} else {

				response.setStatus("NotFound");
			}
		} catch (JsonProcessingException | InterruptedException e) {
			LOGGER.error("process failed");
		}

		return response;
	}

	@Override
	public ResponseEnvelope saveForm(UserDetails userInfo, String id) {
		ResponseEnvelope response = new ResponseEnvelope();
		ValueDetails valueDetails = new ValueDetails();

		try {
			// LOGGER.info("Process result",
			// process.camundaMessage(Constants.loginRequested, id, processVariables));
			UserEntity user = userConverter.convert(userInfo);
			user.setBusinessKey(id);
			userRepo.save(user);

			Map<String, Object> processVariablesR = new HashMap<>();
			valueDetails.setValue("loginFailure");
			valueDetails.setType("String");
			processVariablesR.put("loginStatus", valueDetails);
			LOGGER.info("Process result", process.camundaMessage(Constants.newUser, id, "userLogin", "loginFailed"));
		} catch (Exception e) {
			LOGGER.error("Camunda process failed!!");
		}
		UserEntity user = userRepo.findByFirstNameAndFamilyNameAndDob(userInfo.getFirstName(), userInfo.getFamilyName(),
				userInfo.getDob());

		if (user != null)
			response.setCustomerId(String.valueOf(user.getId()));
		response.setStatus("User Successfully created!!!");
		return response;
	}

	@Override
	public String storeDoc(MultipartFile idCard, MultipartFile addressProof, String isElectronic, String customerId,
			String id) {
		DocumentEntity doc_Entity = new DocumentEntity();
		ValueDetails valueDetails = new ValueDetails();
		Map<String, Object> processVariables = new HashMap<>();
		valueDetails.setValue("documentUpload");
		valueDetails.setType("String");
		processVariables.put("uploadDocStatus", valueDetails);

		try {
			LOGGER.info("Process result", process.camundaMessage(Constants.uploadDoc, id, "documentUpload","uploading"));
			doc_Entity.setAddressProof(new SerialBlob(addressProof.getBytes()));
			doc_Entity.setIdCard(new SerialBlob(idCard.getBytes()));
			doc_Entity.setIsCardElectronic(isElectronic);
			doc_Entity.setCustomerId(customerId);
			doc_Entity.setDocStatus(BusinessConstants.SUBMITED);

			Optional<UserEntity> user = userRepo.findById(Long.valueOf(customerId));
			if (user.isPresent()) {
				if (user.get().getNationality().equalsIgnoreCase(BusinessConstants.COUNTRY)
						&& isElectronic.equalsIgnoreCase(BusinessConstants.YES)) {
					// doc_Entity.setDocStatus(BusinessConstants.AUTOVALIDATED);
					 docRepo.saveAndFlush(doc_Entity);
					successfulValidation(id, valueDetails);
				} else {
					docRepo.saveAndFlush(doc_Entity);
					Map<String, Object> processVariablesR = new HashMap<>();
					valueDetails.setValue("documentAutovalidationFailed");
					processVariablesR.put("docValidation", valueDetails);
					LOGGER.info("Process result",
							process.camundaMessage(Constants.validationFailed, id, "docValidation","docValidationFailed"));
					createTask(user.get());
				}
			}

		} catch (Exception e) {
			LOGGER.info("Document upload failed !!!");
			return "Document upload failed !!!";
		}
		return "Successfully Document uploaded";
	}

	private void successfulValidation(String id, ValueDetails valueDetails)
			throws InterruptedException, JsonMappingException, JsonProcessingException {
		Map<String, Object> processVariablesR = new HashMap<>();
		valueDetails.setValue("documentAutovalidated");
		processVariablesR.put("docValidation", valueDetails);
		LOGGER.info("Process result", process.camundaMessage(Constants.successfulValidation, id, "docValidation","Success"));

	}

	private void createTask(UserEntity userEntity) {
		TaskEntity task = new TaskEntity();
		
		task.setCreationDate(DateFormat.getInstance().format(System.currentTimeMillis()));
		task.setName(String.format("SLV for %s %s", userEntity.getFirstName(), userEntity.getFamilyName()));
		task.setCustomerId(String.valueOf(userEntity.getId()));
		task.setTaskStatus(BusinessConstants.SUBMITED);
		taskRepo.save(task);
	}

	@Override
	public String docStatus(String docStatus, String customerId, String id) {
		Optional<DocumentEntity> doc = docRepo.findById(Long.valueOf(customerId));
		ValueDetails valueDetails = new ValueDetails();
		valueDetails.setType("String");
		try {
			if (doc.isPresent()) {
				DocumentEntity docEntity = doc.get();
				if (docStatus.equalsIgnoreCase(BusinessConstants.YES)) {
					docEntity.setDocStatus(BusinessConstants.Validated);
					docRepo.saveAndFlush(docEntity);
					Map<String, Object> processVariablesR = new HashMap<>();

					valueDetails.setValue("documentFullyValidatedManually");
					processVariablesR.put("docValidation", valueDetails);

					LOGGER.info("Process result",
							process.camundaMessage(Constants.employeeValidation, id, "manualValidation","accepted"));

					return "doc validation Status Updated";
				} else {
					docEntity.setDocStatus(BusinessConstants.Rejected);
					docRepo.saveAndFlush(docEntity);
					Map<String, Object> processVariablesR = new HashMap<>();

					valueDetails.setValue("documentManualValidatationFailed");
					processVariablesR.put("docValidation", valueDetails);

					LOGGER.info("Process result", process.camundaMessage(Constants.docRejected, id, "manalValidation","rejected"));

					return "doc rejection Status Updated";
				}
			}
		} catch (JsonProcessingException | InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "doc validation Status Updated";

	}

	@Override
	public String autoValidation(String businessKey) {
		Optional<UserEntity> user = userRepo.findByBusinessKey(businessKey);
		ValueDetails valueDetails = new ValueDetails();
		valueDetails.setType("String");
		if (user.isPresent() && user.get()!=null && user.get().getNationality().equalsIgnoreCase(BusinessConstants.COUNTRY)) {

			Optional<DocumentEntity> doc = docRepo.findByCustomerId(String.valueOf(user.get().getId()));
			if (doc.isPresent()) {
				DocumentEntity doc_Entity = doc.get();

				doc_Entity.setDocStatus(BusinessConstants.AUTOVALIDATED);
				docRepo.saveAndFlush(doc_Entity);
				UserEntity updateUser = user.get();
				updateUser.setSlvStatus(BusinessConstants.Validated);
				userRepo.save(updateUser);
				// successfulValidation(customerId, valueDetails);
				return "AutoValidated";
			}

		}
		return "AutoValidationFailed";
	}

	@Override
	public List<TaskEntity> retrieveTask(Integer page, Integer size) {
		Pageable paging = PageRequest.of(page, size, Sort.by("creationDate").descending());
		// .and(Sort.by("creationDate").ascending())
		Page<TaskEntity> pageData = taskRepo.findAll(paging);
		return pageData.getContent();
	}

	@Override
	public UserEntity getCustomer(String id) {
		Optional<UserEntity> user = userRepo.findById(Long.valueOf(id));
		if(user.isPresent()) {
			return user.get();
		} else {
			return null;
		}
	}

	@Override
	public DocumentEntity getDocuments(String customerId) {
		Optional<UserEntity> user = userRepo.findById(Long.valueOf(customerId));
		if(user.isPresent()) {
			Optional<DocumentEntity> doc = docRepo.findByCustomerId(customerId);
			if (doc.isPresent()) {
				DocumentEntity temp = doc.get();
				DocumentEntity returnDocument = new DocumentEntity();
				returnDocument.setId(temp.getId());
				returnDocument.setCustomerId(temp.getCustomerId());
				returnDocument.setDocStatus(temp.getDocStatus());
				returnDocument.setIsCardElectronic(temp.getIsCardElectronic());
				return returnDocument;
			}
		}
		return null;
	}
	
	@Override
	public byte[] downloadDocument(String customerId, String file) {
		Optional<UserEntity> user = userRepo.findById(Long.valueOf(customerId));
		if(user.isPresent()) {
			Optional<DocumentEntity> doc = docRepo.findByCustomerId(customerId);
			if (doc.isPresent()) {
				Blob blob = null;
				byte[] data = null;
				
				switch(file.toUpperCase()) {
				case "IDCARD":
					blob = doc.get().getIdCard();
					break;
				case "ADDRESSPROOF":
					blob = doc.get().getAddressProof();
					break;
				}

				if(blob != null) {
					try {
						int blobLength = (int) blob.length();
						data = blob.getBytes(1, blobLength);
					} catch (SQLException e) {
						e.printStackTrace();
					}  
				}

				return data;
			}
		}
		return null;
	}
	
	@Override
	public String documentOrCustomerAcceptance(String businessKey, String taskId, String status) {

		String returnMessage = "";
		Optional<UserEntity> user = userRepo.findByBusinessKey(businessKey);
		UserEntity updateUser = null;
		DocumentEntity updateDoc = null;

		if(user.isPresent()) {
			updateUser = user.get();
			
			Optional<DocumentEntity> doc = docRepo.findByCustomerId(updateUser.getId().toString());
			if(doc.isPresent()) {
				updateDoc = doc.get();
			}
		}
		
		switch(status) {
			case BusinessConstants.DOCUMENT_REJECTED:
				if(updateDoc != null) {
					updateDoc.setDocStatus(BusinessConstants.DOCUMENT_REJECTED);
					docRepo.save(updateDoc);
				}
				returnMessage = "Document Rejected, a mail has been send to the customer.";
				break;
				
			case BusinessConstants.DOCUMENT_ACCEPTED:
				if(updateDoc != null) {
					updateDoc.setDocStatus(BusinessConstants.DOCUMENT_ACCEPTED);
					docRepo.save(updateDoc);

					updateUser.setSlvStatus(BusinessConstants.Validated);
					userRepo.save(updateUser);
				}
				returnMessage = "Document Accepted";
				break;
			
			case BusinessConstants.CUSTOMER_REJECTED:
			default:
				if(user.isPresent()) {
					updateUser.setSlvStatus(BusinessConstants.CUSTOMER_REJECTED);
					userRepo.save(updateUser);
				}
				returnMessage = "Customer Rejected";
				break;
		}
		
		Optional<TaskEntity> task = taskRepo.findById(Long.valueOf(taskId));
		if(task.isPresent()) {
			TaskEntity updateTask = task.get();
			updateTask.setTaskStatus(BusinessConstants.TASK_COMPLETED);
			taskRepo.save(updateTask);
		}
		
		try {
			process.camundaMessage(Constants.documentOrCustomerAcceptance, businessKey, "status", returnMessage);
		} catch (Exception ex) {
			returnMessage += "\nFailed to process";
		}
		
		return returnMessage;
	}
	
	@Override
	public String documentOrSLVStatusCheck(String businessKey) {
		String result = null;

		Optional<UserEntity> user = userRepo.findByBusinessKey(businessKey);
		if(user.isPresent()) {
			//Check if SLV Status
			UserEntity tempUser = user.get();
			if(tempUser.getSlvStatus() == (result = BusinessConstants.CUSTOMER_REJECTED)) {
				return result;
			}

			//Check Document Status
			Optional<DocumentEntity> doc = docRepo.findByCustomerId(tempUser.getId().toString());
			if(doc.isPresent()) {
				DocumentEntity tempDoc = doc.get();
				if(tempDoc.getDocStatus() == (result = BusinessConstants.DOCUMENT_ACCEPTED)) {
					return result;
				} else if(tempDoc.getDocStatus() == (result = BusinessConstants.DOCUMENT_REJECTED)) {
					return result;
				}
			}
		}
		
		//Customer is not present, close loop.
		result = BusinessConstants.CUSTOMER_REJECTED;
		return result; 
	}
}
