/**
 * 
 */
package com.camunda.slv.service;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.camunda.slv.model.DocumentEntity;
import com.camunda.slv.model.ResponseEnvelope;
import com.camunda.slv.model.TaskEntity;
import com.camunda.slv.model.UserDetails;
import com.camunda.slv.model.UserEntity;

/**
 * @author selvark
 *
 */
public interface IProcessService {
	ResponseEnvelope saveForm(UserDetails userInfo,String id);

	ResponseEnvelope checkOnboardingStatus(String firstName, String familyName, String dob,String id);

	String storeDoc(MultipartFile idCard, MultipartFile addressProof, String isElectronic, String customerId, String id);

	String docStatus(String docStatus, String customerId, String id);

	String autoValidation(String businsessKey);

	List<TaskEntity> retrieveTask(Integer page, Integer size);

	UserEntity getCustomer(String id);

	DocumentEntity getDocuments(String customerId);

	byte[] downloadDocument(String customerId, String file);

	String documentOrCustomerAcceptance(String businessKey, String taskId, String status);
	
	String documentOrSLVStatusCheck(String businessKey);
}
