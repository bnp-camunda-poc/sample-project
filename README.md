Spring Boot Web application with Camunda Webapps for SLV flow process

What we developed in this initial pacakge:

Embedded Camunda engine
Camunda web applications automatically deployed
Process application and any number of BPMN process can be deployed ( according to the slv flow process business use case, we have to modify or add new BPMN process )
Admin user configured with login and password configured in application.yaml


We can also put BPMN, CMMN and DMN files in our classpath, it will be automatically deployed and registered within a process application.
Run the application and use Camunda Webapps
You can build the application with mvn clean install and then run it with java -jar command.

Then we can access Camunda Webapps in browser: http://localhost:8081 (provide login/password from application.yaml, default: demo/demo)